import 'package:PapayaApp/DatabaseProducts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'Product.dart';
import 'DetailsItem.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class DisplayProduct extends StatefulWidget {
  final String code;

  const DisplayProduct({this.code});

  @override
  _DisplayProduct createState() => new _DisplayProduct();
}

class _DisplayProduct extends State<DisplayProduct> {
  bool isEnabled = false;
  var valueOfProduct = 0;
  String valueinString = "";
  // ignore: non_constant_identifier_names
  String url_Image = "";
  int index = 0;
  int indexRead = 0;
  String memory = "";
  int selectedIndex = -1;
  ScrollController _controller;
  String selectedName = "";
  Product selectedProductObj;

  List<Product> testProducts = [
    Product(
        id: 10,
        nameProduct: "SILVER ROBOT",
        data1: 10,
        data2: 20,
        data3: 28,
        data4: 5,
        data5: 16,
        data6: 15,
        url: "",
        blocked: false),
    Product(
        id: 11,
        nameProduct: "BLACK ROBOT",
        data1: 5,
        data2: 10,
        data3: 14,
        data4: 5,
        data5: 8,
        data6: 10,
        url: "",
        blocked: false),
    Product(
        id: 12,
        nameProduct: "SELF BALANCE",
        data1: 20,
        data2: 10,
        data3: 28,
        data4: 5,
        data5: 10,
        data6: 10,
        url: "",
        blocked: false),
    Product(
        id: 13,
        nameProduct: "BASIC ROBOT",
        data1: 5,
        data2: 5,
        data3: 5,
        data4: 10,
        data5: 5,
        data6: 20,
        url: "",
        blocked: false),
  ];

  List<Product> getP = [
    Product(
        id: 0,
        nameProduct: "",
        data1: 0,
        data2: 0,
        data3: 0,
        data4: 0,
        data5: 0,
        data6: 0,
        url: "",
        blocked: false),
    Product(
        id: 1,
        nameProduct: "",
        data1: 0,
        data2: 0,
        data3: 0,
        data4: 0,
        data5: 0,
        data6: 0,
        url: "",
        blocked: false),
  ];

  @override
  initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    valueinString = widget.code;
    valueOfProduct = int.parse(valueinString);
    assert(valueOfProduct is int);
  }

  @override
  void dispose() {
    super.dispose();
  }

  enableButton() {
    setState(() {
      isEnabled = true;
    });
  }

  disableButton() {
    setState(() {
      isEnabled = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("History"),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: isEnabled
                    ? () async {
                        await DBProvider.db.deleteProduct(selectedName);
                        setState(() {});
                        print("hola");
                        disableButton();
                      }
                    : () {
                        print("Nothing");
                      },
                child: Icon(
                  Icons.delete_outline,
                  size: 26.0,
                ),
              )),
        ],
      ),
      body: Container(
          child: ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: <Widget>[
            /*Divider(),
            ListTile(
              title: ElevatedButton(
                onPressed: () async {
                  try {
                    await DBProvider.db.newProduct(testProducts[index]);
                    int test = testProducts[index].id;
                    String test2 = testProducts[index].nameProduct;
                    print('el ID del nuevo producto $test');
                    print('el Nombre del nuevo producto $test2');
                    setState(() {});
                  } catch (err) {
                    print('error al agregar: $err');
                  }

                  if (index == 3) {
                    index = 0;
                  } else {
                    index++;
                  }

                  //Navigator.pop(context); to go back
                },
                child: Text('Agregar item a la tabla'),
              ),
            ),*/
            Divider(),
            /*ListTile(
              title: ElevatedButton(
                onPressed: () async {
                  try {
                    getP = await DBProvider.db.getAllProductsfromCellphone();

                    String idadded = getP[indexRead].nameProduct;
                    print('producto1 aderido:$idadded $indexRead');
                    setState(() {});
                    //print(testProducts);
                    //Navigator.pop(context); to go back
                  } catch (err) {
                    print('error de leer el ultimo item:$err');
                  }
                  if (indexRead == 5) {
                    indexRead = 0;
                  } else {
                    indexRead++;
                  }
                },
                child: Text('leer el ultimo item!'),
              ),
            ),
            Divider(),*/
            ListTile(
              title: ElevatedButton(
                onPressed: () async {
                  try {
                    await DBProvider.db.deleteAll();
                    print('Remove ALL');
                    setState(() {});
                  } catch (err) {
                    print('error de borrar : $err');
                  }

                  //print(testProducts);
                  //Navigator.pop(context); to go back
                },
                child: Text('Borrar la table'),
              ),
            ),
            Divider(),
            /*ListTile(
              title: ElevatedButton(
                onPressed: () async {
                  try {
                    memory = getP[indexRead].nameProduct;
                    print('producto1 aderido:$memory $indexRead');
                    setState(() {});
                  } catch (err) {
                    print('error de leer item actual : $err');
                  }
                  //print(testProducts);
                  //Navigator.pop(context); to go back
                },
                child: Text('leer el item actual $memory $indexRead'),
              ),
            ),
            Divider(),*/
            FutureBuilder<List<Product>>(
                future: DBProvider.db.getAllProductsfromCellphone(),
                //initialData: getP,
                builder: (BuildContext context,
                    AsyncSnapshot<List<Product>> snapshot) {
                  return snapshot.hasData
                      ? ListView.builder(
                          controller: _controller,
                          //physics: ,
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int position) {
                            Product product = snapshot.data[position];
                            bool getbt = product.blocked;
                            String getnameP = product.nameProduct;
                            return InkWell(
                                onTap: () =>
                                    _displayItemDetail(context, product),
                                onLongPress: () => setState(() {
                                      selectedIndex = position;
                                      selectedName = getnameP;
                                      print("$selectedIndex");
                                      print("$selectedName");
                                      enableButton();
                                    }),
                                child: Card(
                                    shape: (selectedIndex == position)
                                        ? RoundedRectangleBorder(
                                            side:
                                                BorderSide(color: Colors.green))
                                        : null,
                                    elevation: 5,
                                    child: Row(
                                      children: [
                                        Image.network(
                                          product.url,
                                          width: 150,
                                          height: 150,
                                        ),
                                        Flexible(
                                          child: ListTile(
                                            title: Text(product.nameProduct),
                                          ),
                                        ),
                                        Flexible(
                                          child: ListTile(
                                            title: Text('$getbt'),
                                          ),
                                        ),
                                      ],
                                    )));
                          },
                        )
                      : Center(
                          child: CircularProgressIndicator(),
                        );
                })
          ])),
      floatingActionButton: FloatingActionButton(
          onPressed: () => scanQR(),
          child: const Icon(Icons.qr_code),
          backgroundColor: Colors.black),
    );
  }

  void _displayItemDetail(BuildContext context, Product product) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return DetailsItem(productSelected: product);
        },
      ),
    );
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the bottom";
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        // message = "reach the top";
      });
    }
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      valueOfProduct = int.parse(barcodeScanRes);
      print('$valueOfProduct');
    });

    Product robotDetected;
    assert(valueOfProduct is int);

    switch (valueOfProduct) {
      case 2:
        print("2");
        robotDetected = new Product(
            id: 12,
            nameProduct: "SELF BALANCE",
            data1: 20,
            data2: 10,
            data3: 28,
            data4: 5,
            data5: 10,
            data6: 10,
            url:
                "https://ae01.alicdn.com/kf/H9589748770994afb836ca5fa934f2058B/Yahboom-arduino-RTR-programmable-bluetooth-smart-balance-car-STM32-DIY-self-balance-intelligent-robot-kit-RC.jpg_Q90.jpg_.webp",
            blocked: false);

        break;
      case 4:
        print("4");
        robotDetected = new Product(
            id: 10,
            nameProduct: "SILVER ROBOT",
            data1: 10,
            data2: 20,
            data3: 28,
            data4: 5,
            data5: 16,
            data6: 15,
            url:
                "https://ae01.alicdn.com/kf/HTB1VTMZwv9TBuNjy1zbq6xpepXaY/DOF-robot-humanoid-17-robot-ducatif-haut-de-gamme-comp-titif-avec-engrenage-en-m-tal.jpg_Q90.jpg_.webp",
            blocked: false);

        break;
      case 6:
        print("6");
        robotDetected = new Product(
            id: 13,
            nameProduct: "BASIC ROBOT",
            data1: 5,
            data2: 5,
            data3: 5,
            data4: 10,
            data5: 5,
            data6: 20,
            url:
                "https://ae01.alicdn.com/kf/H0ec9da409c344c549053aa4d566a348cc/RC-Two-Wheel-Self-Balancing-Robot-Car-Chassis-Kit-with-Dual-DC-12V-Motor-with-Speed.jpg_Q90.jpg_.webp",
            blocked: false);

        break;
      case 8:
        robotDetected = Product(
            id: 11,
            nameProduct: "BLACK ROBOT",
            data1: 5,
            data2: 10,
            data3: 14,
            data4: 5,
            data5: 8,
            data6: 10,
            url:
                "https://ae01.alicdn.com/kf/Hb98749e2b70d4452a7f1e2b0f64baa135/13-DOF-Humanoid-Robot-Biped-Walking-Dance-Robot-Robot-Competition-Accessories-15-degrees-of-freedom-robot.jpg_Q90.jpg_.webp",
            blocked: false);
        print("8");

        break;
      default:
        print("NO DATA");
        return;
        break;
    }

    try {
      await DBProvider.db.newProduct(robotDetected);
      int test = robotDetected.id;
      String test2 = robotDetected.nameProduct;
      print('el ID del nuevo producto $test');
      print('el Nombre del nuevo producto $test2');
      setState(() {});
    } catch (err) {
      print('error al agregar: $err');
    }
  }
}
